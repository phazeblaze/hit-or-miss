extends Area2D

func _on_MissArea_body_entered(body):
	if "Circle" in body.get_name():
		global.miss += 1
		global.combo = 0
		body.queue_free()
