extends Node2D

var redCircle = load("Scenes/redCircle.tscn")
var blueCircle = load("Scenes/blueCircle.tscn")
var testPattern = load("Scenes/testPattern.tscn")
var counter = 0

func _process(delta):
	pass
	
func _ready():
	pass

func spawnCircles0():
	$Spawn1/Spawnpoint1.set_offset(0)
	$Spawn2/Spawnpoint1.set_offset(0)
	var newCircle = redCircle.instance()
	var newCircle2 = blueCircle.instance()
	add_child(newCircle)
	add_child(newCircle2)

func spawnCircles1():
	$Spawn1/Spawnpoint1.set_offset(0)
	for n in range(3):
		var newCircle = testPattern.instance()
		newCircle.position += Vector2(n*150, 0)
		add_child(newCircle)
		
	
func spawnCircles3():
	var newCircle = testPattern.instance()
	add_child(newCircle)	
	

func _on_SpawnTimer_timeout():
	spawnCircles3()
	$SpawnTimer2.start()


func _on_SpawnTimer2_timeout():
	spawnCircles3()
