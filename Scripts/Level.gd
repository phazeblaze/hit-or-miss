extends Node2D

var redCircle = load("Scenes/redCircle.tscn")
var blueCircle = load("Scenes/blueCircle.tscn")
var levelMap = load("Scenes/LevelMap.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func spawnLevel():
	var level = levelMap.instance()
	add_child(level)

func _on_startTimer_timeout():
	 spawnLevel()

func _on_stageTimer_timeout():
	get_tree().change_scene(str("res://Scenes/Results.tscn"))
