extends Label

func _process(delta):
	self.text = "Hit : " + str(global.perfect) + "\nMiss : " + str(global.miss) + "\n\nCombo : " + str(global.combo)
	
	if global.combo > global.maxcombo:
		global.maxcombo = global.combo
