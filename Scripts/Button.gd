extends LinkButton

export(String) var scene_to_load

func _on_LinkButton_pressed():
	global.perfect = 0
	global.great = 0
	global.nice = 0
	global.miss = 0
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_LinkButton2_pressed():
	global.perfect = 0
	global.great = 0
	global.nice = 0
	global.miss = 0
	get_tree().change_scene(str("res://Scenes/Loading.tscn"))
