extends MarginContainer


func _ready():
	$tick.play()

func _process(delta):
	var timer = $Timer
	var timerNumber = $HBoxContainer/VBoxContainer/Label2
	timerNumber.text = str(ceil(timer.get_time_left()))

func _on_Timer_timeout():
	get_tree().change_scene(str("res://Scenes/Level.tscn"))


func _on_Timer2_timeout():
	$tick.play()


func _on_Timer3_timeout():
	$tick.play()
