extends Node

var entered = 0
var entered_great = 0
var entered_perfect = 0
var circle

func _process(delta):
	if entered_perfect == 1:
		if ("red" in (circle.get_name()).to_lower()) and Input.is_action_just_pressed("buttonOne"):
			global.perfect += 1
			global.combo += 1
			$tapSound2.playing = true
			circle.queue_free()
		if ("blue" in (circle.get_name()).to_lower()) and Input.is_action_just_pressed("buttonTwo"):
			global.perfect += 1
			global.combo += 1
			$tapSound2.playing = true
			circle.queue_free()
